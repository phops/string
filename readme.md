
Phops String
============


Basic usage
-----------

```bash
composer require phops/string
```


License
-------

Phops JSON is licensed under the [MIT license](/license.txt).
