<?php

namespace Phops;

class URL {

  static function follow ($base, $url) {
    if (URL::isPath($base) && URL::isPath($url)) {
      if (!URL::isRelativePath($url))
        return $url;
      return (in_array(substr($base, -1), ['/', '\\']) ? $base : $base . '/') . $url;
    }
  }

  static function isPath ($url) {
    if (false !== strpos($url, '://')) {
      return false;
    }
    //if (in_array(substr($url, 1, 2), [':/', ':\\']) && substr($url, 1, 3) != '://')
    //  return true;
    //if (strpos($url, ':') !== false && strpos($url, ':') < strpos($url, '/'))
    //  return false;
    return true;
  }

  static function isRelativePath ($path) {
    if (strpos($path, '/') === 0)
      return false;
    if (strpos($path, '\\') === 0)
      return false;
    if (in_array(substr($path, 1, 2), [':/', ':\\']))
      return false;
    return true;
  }

}
