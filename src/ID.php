<?php

namespace Phops;

class ID {

  static function unique () {
    $bits = 144;
    $bytes = ceil($bits / log(36, 2));
    return substr(strtolower(str_replace(['+', '/', '='], '', base64_encode(random_bytes($bytes)))), 0, $bytes);
  }

}
